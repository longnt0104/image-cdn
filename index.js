import * as fs from 'node:fs';

// const mkdirp = require('mkdirp');
import mkdirp from 'mkdirp';
import path from 'path';
// const getDirName = require('path').dirname;
import imagemin from 'imagemin';
import imageminWebp from 'imagemin-webp';
import imageminMozjpeg from 'imagemin-mozjpeg';
// const imageminPngquant = require('imagemin-pngquant');
import imageminPngquant from 'imagemin-pngquant';

const getDirName = path.dirname;
const config = {
  sourceDir: 'images',
  destinationDir: 'public',
  preserveDirectoryStructure: true,
  patterns: [
    { // compress jpegs
      pattern: '/**/*.{jpg,jpeg}',
      plugins: [
        imageminMozjpeg(),
      ]
    },
    { // compress pngs
      pattern: '/**/*.{png}',
      plugins: [
        imageminPngquant({ quality: [0.7, 0.8] }),
      ]
    },
    { // convert all images to webp
      pattern: '/**/*.{jpg,jpeg,png}',
      plugins: [
        imageminWebp({quality: 70}),
      ]
    }
  ]
}

const flattenDeep = (arr) => {
   return arr.reduce((acc, val) => Array.isArray(val) ? acc.concat(flattenDeep(val)) : acc.concat(val), []);
}

const saveFile = (path, buffer) => {
  return new Promise((resolve, reject) => {
    mkdirp(getDirName(path), function (err) {
      if (err) return cb(err);

      fs.writeFile(path, buffer, (err, result) => {
        if (err) reject('error writing file: ' + err);
        else resolve(result);
      });
    });
  })
}

Promise.all(
  config.patterns.map(({plugins, pattern}) => {
    const options = { plugins };
    const glob = [`${config.sourceDir}${pattern}`]
    if (!config.preserveDirectoryStructure) {
      options.destination = config.destinationDir;
    }
    return imagemin(glob, options);
  }),
)
.then(flattenDeep)
.then(files => Promise.all(
  files.map((file) => saveFile(
    file.sourcePath.replace('images', config.destinationDir),
    file.data
  ))
))
.catch(console.error);